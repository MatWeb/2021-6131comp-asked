#define PIR_SENSOR                    (35)
#define HIGH_TEMP_POT                 (33)
#define LOW_TEMP_POT                  (32)
#define LED_RGB_R                     (25)
#define LED_RGB_G                     (26)
#define LED_RGB_B                     (27)
#define DHTPIN                        (4)
#define DHTTYPE                       DHT11
#define PIR_DETECT_STATE              (1)

#define ONE_MHZ_FREQ_DIV              (80)
#define SECOND_TO_MICRO(X)            (X * 1000000)
#define LED_FLASH_SECONDS             (1)
#define SERIAL_LOG_SECONDS            (5)
#define VACANT_THRSHOLD_MINUTES       (10)
#define VACANT_THRESHOLD_COUNT        (60 * VACANT_THRSHOLD_MINUTES/SERIAL_LOG_SECONDS)


#define HIGH_TEMPERATURE_C_THRESHOLD  (28)
#define LOW_TEMPERATURE_C_THRESHOLD   (10)
#define COMMON_ANODE_RGB_LED          (0)

#define LED_FLASH_TIMER_ID            (0)
#define SERIAL_LOG_TIMER_ID           (1)

#include <Arduino.h>
#include <analogWrite.h>
#include "DHT.h"

enum LED_COLOR
{
  NO_COLOR,
  RED,
  BLUE,
  GREEN,
  ORANGE
};

struct DHTData
{
  float fHumidity;
  float fTemperature_C;
  float fTemperature_F;
};

struct SensorData
{
  DHTData dhtData;
  bool occupancy_bool;
};

struct ThresholdData
{
  float fHighTemperature_C;
  float fLowTemperature_C;
};


SensorData sensorData;
ThresholdData thresholdData ={
  .fHighTemperature_C = HIGH_TEMPERATURE_C_THRESHOLD,
  .fLowTemperature_C = LOW_TEMPERATURE_C_THRESHOLD
};

LED_COLOR ledColor = NO_COLOR;
hw_timer_t * tLedFlashTimer = NULL;
hw_timer_t * tSerialLogTimer = NULL;
bool isTimerEnabled = false;
int iVacantCounter_glb = false;

DHT dht(DHTPIN, DHTTYPE);

void toggleLED()
{
  static bool ledState = true;
  isTimerEnabled = true;
  ledState = !ledState;

  if(ledState)
  {
    RGB_color(255,35,0);
  }
  else
  {
    RGB_color(0,0,0);
  }
}

void logSerial()
{
  Serial.println("==============================================================================");
  Serial.print("Humidity: ");
  Serial.print(sensorData.dhtData.fHumidity);
  Serial.print("\tTemperature(F):");
  Serial.print(sensorData.dhtData.fTemperature_F);
  Serial.print("\tTemperature(C): ");
  Serial.print(sensorData.dhtData.fTemperature_C);
  Serial.print("\tOcuupied: ");
  Serial.print(sensorData.occupancy_bool);
  Serial.println();
  Serial.print("High temperature threshold (C): ");
  Serial.print(thresholdData.fHighTemperature_C);
  Serial.print("\tLow temperature threshold (C): ");
  Serial.println(thresholdData.fLowTemperature_C);
  Serial.println("==============================================================================");
  iVacantCounter_glb++;

  if (iVacantCounter_glb == VACANT_THRESHOLD_COUNT)
  {
    iVacantCounter_glb = 0;
    sensorData.occupancy_bool = false;
    Serial.println("Building is VACANT");
  }
}

void runTest()
{
   Serial.println("Initiating to procedure to test RGB LED, PIR Sensor, Temperature Sensor");
   Serial.println("------------------------------------------------------------------------");
   Serial.println("RGB LED TEST");
   Serial.println("------------------------------------------------------------------------");
   Serial.println("Lighting up led for red colour");
   RGB_color(255,0,0);
   delay(5000);
   Serial.println("Lighting up led for green colour");
   RGB_color(0,255,0);
   delay(5000);
   Serial.println("Lighting up led for blue colour");
   RGB_color(0,0,255);
   delay(5000);
   Serial.println("Lighting up led for orange colour");
   RGB_color(255,35,0);
   delay(5000);
   Serial.println("DONE : RGB LED TEST");
   Serial.println("------------------------------------------------------------------------");
   Serial.println("TEMPERATURE SENSOR TEST");
   Serial.println("------------------------------------------------------------------------");
   
   
   readDHT();
   Serial.println();
    
   Serial.println("DONE : TEMPERATURE SENSOR TEST");
   Serial.println("------------------------------------------------------------------------");
   Serial.println("PIR SENSOR TEST");
   Serial.println("------------------------------------------------------------------------");
   Serial.println("Wave your hand around the PIR sensor");
   while(digitalRead(PIR_SENSOR) != PIR_DETECT_STATE);
   Serial.println("DONE : PIR SENSOR TEST");

   Serial.println("DONE TESTTING RGB LED, PIR SENSOR, TEMPERATURE SENSOR");
}

void RGB_color(int red_light_value, int green_light_value, int blue_light_value)
 {
  red_light_value = COMMON_ANODE_RGB_LED? 255 - red_light_value : red_light_value;
  green_light_value = COMMON_ANODE_RGB_LED? 255 - green_light_value : green_light_value;
  blue_light_value = COMMON_ANODE_RGB_LED? 255 - blue_light_value : blue_light_value;

  analogWrite(LED_RGB_R, red_light_value);
  analogWrite(LED_RGB_G, green_light_value);
  analogWrite(LED_RGB_B, blue_light_value);
}

void readDHT()
{
  // Wait a few seconds between measurements.
  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  
  sensorData.dhtData.fHumidity = dht.readHumidity();
  
  // Read temperature as Celsius (the default)
  sensorData.dhtData.fTemperature_C = dht.readTemperature();
 
  // Read temperature as Fahrenheit (isFahrenheit = true)
  sensorData.dhtData.fTemperature_F = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(sensorData.dhtData.fHumidity) || isnan(sensorData.dhtData.fTemperature_C) || isnan(sensorData.dhtData.fTemperature_F)) {
    Serial.println(F("Failed to read from DHT sensor!"));
}
}

void readSensors()
{
  int iHighThreshold, iLowThreshold;
  readDHT();
  if(digitalRead(PIR_SENSOR) == PIR_DETECT_STATE)
  {
    if (!sensorData.occupancy_bool)
    {
      Serial.println("Building is OCCUPIED");
    }
    iVacantCounter_glb = 0;
    sensorData.occupancy_bool = true;
  }
/*
  iHighThreshold = map(analogRead(HIGH_TEMP_POT), 0, 4096, LOW_TEMPERATURE_C_THRESHOLD, HIGH_TEMPERATURE_C_THRESHOLD);
  iLowThreshold = map(analogRead(LOW_TEMP_POT), 0, 4096, LOW_TEMPERATURE_C_THRESHOLD, HIGH_TEMPERATURE_C_THRESHOLD);

  if (iHighThreshold > iLowThreshold)
  {
    thresholdData.fHighTemperature_C = iHighThreshold;
    thresholdData.fLowTemperature_C = iLowThreshold;
  }
  else if (iHighThreshold <= iLowThreshold)
  {
    Serial.println("\nHigh temperature threshold should be greater than lower temperature threshold. Please increase high temperature threshold or reduce low temperature threshold");
  }
  else
  {
    
  }
*/

  
}

void setup() {
  // put your setup code here, to run once:
   DHTData dhtData;
   tLedFlashTimer = timerBegin(LED_FLASH_TIMER_ID, ONE_MHZ_FREQ_DIV, true);
   tSerialLogTimer = timerBegin(SERIAL_LOG_TIMER_ID, ONE_MHZ_FREQ_DIV, true);
   Serial.begin(115200);
   pinMode(LED_RGB_R, OUTPUT);
   pinMode(LED_RGB_G, OUTPUT);
   pinMode(LED_RGB_B, OUTPUT);
   pinMode(PIR_SENSOR, INPUT);
   dht.begin();

   //runTest()
   
   timerAttachInterrupt(tLedFlashTimer, &toggleLED, true);
   timerAttachInterrupt(tSerialLogTimer, &logSerial, true);
   timerAlarmWrite(tLedFlashTimer, SECOND_TO_MICRO(LED_FLASH_SECONDS), true);
   timerAlarmWrite(tSerialLogTimer, SECOND_TO_MICRO(SERIAL_LOG_SECONDS), true);

   timerAlarmEnable(tSerialLogTimer);
  RGB_color(0,0,0);
   
}

void loop() {
  // put your main code here, to run repeatedly:
  
  readSensors();

  if(sensorData.dhtData.fTemperature_C > thresholdData.fHighTemperature_C && sensorData.occupancy_bool)
  {
    // RED Light
    if(isTimerEnabled)
    {
      timerAlarmDisable(tLedFlashTimer);
      isTimerEnabled = false;
    }
    if (ledColor != RED)
    {
      Serial.println("Lighting up led for red colour");
      RGB_color(255,0,0);
      ledColor = RED;
    }
  
    
  }

  else if(sensorData.dhtData.fTemperature_C < thresholdData.fLowTemperature_C && sensorData.occupancy_bool)
  {
    // Blue Light
    if(isTimerEnabled)
    {
      timerAlarmDisable(tLedFlashTimer);
      isTimerEnabled = false;
    }

    if (ledColor != BLUE)
    {
      Serial.println("Lighting up led for blue colour");
      RGB_color(0,0,255);
      ledColor = BLUE;
    }    
  }
  else if (sensorData.occupancy_bool)
  {
    // Green Light
    if(isTimerEnabled)
    {
      timerAlarmDisable(tLedFlashTimer);
      isTimerEnabled = false;
    }
    if (ledColor != GREEN)
    {
      Serial.println("Lighting up led for green colour");
      RGB_color(0,255,0);
      ledColor = GREEN;
    }

    
  }
  else
  {
    // Orange Light
    if (!isTimerEnabled)
    {
      Serial.println("Flashing up led for orange colour");
      timerAlarmEnable(tLedFlashTimer);
      ledColor = ORANGE;
    }
  }

  delay(100);
}
